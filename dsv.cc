#include <grpcpp/grpcpp.h>
#include <string>
#include <thread>
#include <chrono>
#include <mutex>
#include <vector>
#include <sstream>
#include <condition_variable>
#include "dsv.grpc.pb.h"
#include <fstream>
#include <ctime>


using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;

using dsv::dsvRequest;
using dsv::dsvResponse;
using dsv::Dsv;


using namespace std;


class dsvNode : public Dsv::Service
{
private:
    int nodeId;
    mutex m;
    mutex m2;
    // stub
    string leftNeighborAddress = "";
    unique_ptr<Dsv::Stub> leftStub;
    shared_ptr<Channel> leftChannel;
    string rightNeighborAddress = "";
    unique_ptr<Dsv::Stub> rightStub;
    shared_ptr<Channel> rightChannel;
    // skeleton
    string leftSkeletonAddress = "";
    string rightSkeletonAddress = "";
    thread *leftTSkeleton = NULL;
    thread *rightTSkeleton = NULL;
    unique_ptr<Server> leftSkeleton;
    unique_ptr<Server> rightSkeleton;
    int lId;
    int rId;
    bool lReady;
    bool rReady;
    int lowestDetected;
    bool got_left_message_back;
    bool got_right_message_back;
    int messageCount;
    int awaken;

public:
    dsvNode(int nodeId = 0, string leftSkeletonAddress = "", string rightSkeletonAddress = "",
            string leftNeighborAddress = "", string rightNeighborAddress = "", int nodes = 0, int lId = 0, int rId = 0)
    {
        this->nodeId = nodeId;
        this->leftSkeletonAddress = leftSkeletonAddress;
        this->rightSkeletonAddress = rightSkeletonAddress;
        this->leftNeighborAddress = leftNeighborAddress;
        this->rightNeighborAddress = rightNeighborAddress;
        this->lId = lId;
        this->rId = rId;
        this->lReady = false;
        this->rReady = false;
        this->got_left_message_back = false;
        this->got_right_message_back = false;
        this->lowestDetected = nodeId;
        this->awaken = false;
        this->messageCount = 0;
    }

    int getLeader() {
        return this->lowestDetected;
    }

    int getMessageCount() {
        return this->messageCount;
    }

    int isAwaken() {
        return this->awaken;
    }

    int get_id() {
        return this->nodeId;
    }

    // send messag to neighbor stub
    void sendLeftMessage(int ownerid, int ttl)
    {
        // assemble request
        this->messageCount ++;
        dsvRequest request;
        request.set_ownerid(ownerid);
        request.set_receiverid(lId);
        request.set_senderid(nodeId);
        request.set_ttl(ttl - 1);
        time_t now = time(nullptr);
        tm * local = localtime(&now);
        cout << "<" << nodeId << ", " << local->tm_hour << ":" << local->tm_min << ":" << local->tm_sec <<
             ", " << request.ownerid() << ", " << request.receiverid() << ">\n";
        // container for server response
        dsvResponse reply;
        // Context can be used to send meta data to server or modify RPC behaviour
        ClientContext context;
        // Actual Remote Procedure Call
        Status status = leftStub->sendLeftMessage(&context, request, &reply);
        // Returns results based on RPC status
        if (status.ok()) {
        } else {
            //cout << status.error_code() << ": " << status.error_message() << endl;
        }
    }

    // receives message from neighbor stub
    Status sendLeftMessage(ServerContext* context, const dsvRequest* request, dsvResponse* reply) override
    {
        if (request->ownerid() == nodeId) {
            got_right_message_back = true;
            return Status::OK;
        }
        if (request->ownerid() > lowestDetected) {
            return Status::OK;
        }
        else {
            lowestDetected = request->ownerid();
        }
        // message is for this node
        if (request->ttl() == 0) {
            m2.lock();
            sendRightMessage(request->ownerid(), INT32_MAX);
            m2.unlock();
            reply->set_code(0);
        } else {
            m2.lock();
            sendLeftMessage(request->ownerid(), request->ttl());
            m2.unlock();
            reply->set_code(1);
        }
        return Status::OK;
    }

    // send messag to neighbor stub
    void sendRightMessage(int ownerId, int ttl)
    {
        // assemble request
        this->messageCount ++;
        dsvRequest request;
        request.set_ownerid(ownerId);
        request.set_receiverid(rId);
        request.set_senderid(nodeId);
        request.set_ttl(ttl - 1);
        time_t now = time(nullptr);
        tm * local = localtime(&now);
        cout << "<" << nodeId << ", " << local->tm_hour << ":" << local->tm_min << ":" << local->tm_sec <<
             ", " << request.ownerid() << ", " << request.receiverid() << ">\n";// container for server response
        dsvResponse reply;
        // Context can be used to send meta data to server or modify RPC behaviour
        ClientContext context;
        // Actual Remote Procedure Call
        Status status = rightStub->sendRightMessage(&context, request, &reply);
        // Returns results based on RPC status
        if (status.ok()) {
        } else {
            //cout << status.error_code() << ": " << status.error_message() << endl;
        }
    }

    // receives message from neighbor stub
    Status sendRightMessage(ServerContext* context, const dsvRequest* request, dsvResponse* reply) override
    {
        if (request->ownerid() == nodeId) {
            got_left_message_back = true;
            return Status::OK;
        }
        if (request->ownerid() > lowestDetected) {
            return Status::OK;
        }
        else {
            lowestDetected = request->ownerid();
        }
        if (request->ttl() == 0) {
            m2.lock();
            sendLeftMessage(request->ownerid(), INT32_MAX);
            m2.unlock();
            reply->set_code(0);
        } else {

            m.lock();
            sendRightMessage(request->ownerid(), request->ttl());
            m.unlock();
            reply->set_code(1);
        }
        return Status::OK;
    }

    void wakeNode(int max_ttl)
    {
        int ttl = 1;

        bool sendNextWave = true;
        this->awaken = true;

        while (sendNextWave) {
            got_left_message_back = false;
            got_right_message_back = false;
            m.lock();
            sendRightMessage(nodeId, ttl);
            m.unlock();

            m2.lock();
            sendLeftMessage(nodeId, ttl);
            m2.unlock();
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            sendNextWave = got_left_message_back && got_right_message_back;
            if (ttl == max_ttl) {
                break;
            }
            ttl += 1;
        }
    }

    void createRightSkeleton()
    {
        string right_server_address(rightSkeletonAddress);
        ServerBuilder builder;
        // Listen on the given address without any authentication mechanism
        builder.AddListeningPort(right_server_address, grpc::InsecureServerCredentials());
        // Register "service" as the instance through which
        // communication with client takes place
        builder.RegisterService(this);
        // Assembling the skeleton interface
        rightSkeleton = builder.BuildAndStart();
        //cout << "Server listening on port: " << rightSkeletonAddress << endl;
        // server started;lowestDetected

        rReady = true;
        rightSkeleton->Wait();

    }

    void shutdown()
    {
        this->rightSkeleton->Shutdown();
        this->leftSkeleton->Shutdown();
    }

    void createLeftSkeleton() {
        string left_server_address(leftSkeletonAddress);
        ServerBuilder builder;
        // Listen on the given address without any authentication mechanism
        builder.AddListeningPort(left_server_address, grpc::InsecureServerCredentials());
        // Register "service" as the instance through which
        // communication with client takes place
        builder.RegisterService(this);
        // Assembling the skeleton interface
        leftSkeleton = builder.BuildAndStart();
        //cout << "Server listening on port: " << leftSkeletonAddress << endl;
        // server started;

        lReady = true;
        leftSkeleton->Wait();
    }

    // creating a single thread serving as skeleton
    void startSkeleton()
    {
        leftTSkeleton = new thread(&dsvNode::createLeftSkeleton, this);
        rightTSkeleton = new thread(&dsvNode::createRightSkeleton, this);
        // wait until the thread is ready
        while (lReady != true && rReady != true) {}
        //cout << "Skeleton " << nodeId << " is ready." << endl;
    }

    void startStub()
    {
        string left_target_address(leftNeighborAddress);
        // Instantiates the client
        leftChannel =  grpc::CreateChannel(left_target_address, grpc::InsecureChannelCredentials());
        leftStub = Dsv::NewStub(leftChannel);

        string right_target_address(rightNeighborAddress);
        // Instantiates the client
        rightChannel =  grpc::CreateChannel(right_target_address, grpc::InsecureChannelCredentials());
        rightStub = Dsv::NewStub(rightChannel);
    }

};

int main(int argc, char* argv[])
{
    int portNum = 60000;
    string ip = "127.0.0.1";
    vector<shared_ptr<dsvNode> > ring;
    stringstream ss;
    string line;
    ofstream outFile;
    outFile.open("res.txt");
    int i = 0;
    while (getline(cin, line)) {
        istringstream iss (line);
        std::vector<int> nodeIds;
        string idStr;
        int lowest = INT32_MAX;
        while (getline(iss, idStr, ',')) {
            int newId = atoi(idStr.c_str());
            lowest = min(lowest, newId);
            nodeIds.push_back(newId);
        }
        int nodeCount = nodeIds.size();
        // creating nodes
        for (int i=0; i < nodeCount; i++) {
            ss << ip << ":" << portNum+i;
            string rightSkeletonAddress = ss.str();
            ss.str("");
            ss << ip << ":" << portNum+nodeCount+i;
            string leftSkeletonAddress = ss.str();
            ss.str("");
            ss << ip << ":" << portNum+(i+1)%(nodeCount);
            string rightNeighborAddress = ss.str();
            ss.str("");
            int leftAddrI = portNum+(i-1)%(nodeCount);
            if (leftAddrI < 60000) {
                leftAddrI += 2 * nodeCount;
            }
            int leftNode = (i - 1) % nodeCount;
            if (leftNode < 0) {
                leftNode += nodeCount;
            }
            ss << ip << ":" << leftAddrI;
            string leftNeighborAddress = ss.str();
            ss.str("");
            ring.emplace_back(make_shared<dsvNode>(nodeIds[i], leftSkeletonAddress,rightSkeletonAddress,
                                                   leftNeighborAddress, rightNeighborAddress, nodeCount, nodeIds[leftNode], nodeIds[(i + 1) % nodeCount]));
        }
        // starting skeletons
        for (auto node : ring) node->startSkeleton();
        // starting stubs
        for (auto node : ring) node->startStub();
        int started = 0;
        while (started < nodeCount) {
            int now_awaken = rand() % nodeCount;
            if (!ring[now_awaken].get()->isAwaken()) {
                ring[now_awaken]->wakeNode(nodeCount / 2);
                started ++;
            }
        }
        int finalMessageCount = 0;
        for (int i = 0; i < nodeCount; i ++) {
            finalMessageCount += ring[i].get()->getMessageCount();
            ring[i].get()->shutdown();
            assert(lowest == ring[i].get()->getLeader());
        }
        outFile << finalMessageCount << '\n';

        ring.clear();
        if (i ++ == 100) {
            break;
        }
    }
    outFile.close();
    return 0;
}